let numero=[];
let cnt=0;
let mayor=0
let par=0;

// Utilizamos un contador para asignar 10 variables, dese el 0 hasta el 9.
while(cnt<10){
    numero[cnt]=+prompt("Introduce un numero");
    cnt+=1;
}

for (cnt=0; cnt<=9; cnt++) {
    if (numero[cnt]%2==0) {
       par++
    }
}
document.write(`Entre los numeros introducios hay ${par} que son pares.<br>`);

/* Una forma de calcular el numero mayor entre muchas variables.
mayor=numero[0];
for (cnt=0; cnt<=9; cnt++) {
    if (numero[cnt]>mayor) {
        mayor=numero[cnt];
    }
}
*/

// Otra forma de calcular el numero mayor o el menor es usando esta funcion.
numero.sort(function(a,b){return b-a});
mayor=numero[0];
document.write(`${mayor} es el numero mayor.<br>`); 

numero.sort(function(a,b){return a-b});
mayor=numero[0];
document.write(`${mayor} es el numero menor.`); 
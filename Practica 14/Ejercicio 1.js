let palabras = ["codigo", "archivo", "programa", "imagen", "dato", "funcion", "variable", "constante", "navegador", "pagina"];
let numero = Math.floor((Math.random() * palabras.length));
let palabra = palabras[numero];
let tabla = document.querySelector('tr');
let boton = document.querySelector('button');
let letra = document.querySelector('input');
let victoria = 0;

Array.from(palabra);

for (let cont = 0; cont < palabra.length; cont++) {
    if (cont % 2 == 0) {
        tabla.innerHTML += `<td></td><br>`;
        victoria++;
    } else {
        tabla.innerHTML += `<td>${palabra[cont]}</td><br>`;
    }
}

let cimagen = 6;
let fallo = 0;
let acierto = 0;
let puntos = 0;

boton.addEventListener("click", function comprobar() {
    let td = document.querySelectorAll('td');
    let imagen = document.querySelector('img');

    for (let cont = 0; cont < palabra.length; cont++) {
        if (letra.value == td[cont].innerHTML) {
            fallo = 0;
            acierto++;
            puntos -= 1;
        } else if (letra.value == palabra[cont]) {
            td[cont].innerHTML = `${letra.value}`;
            acierto++;
            fallo = 0;
        } else if (acierto > 0) {
            fallo = 0;
        } else {
            fallo++;
        }
    }

    if (fallo >= 1) {
        cimagen -= 1;
        fallo = 0;
    } else if (acierto == 1) {
        acierto = 0;
        puntos++;
    }
    else if (acierto > 1) {
        puntos += acierto;
        acierto = 0;
    } else {
        acierto = 0;
    }
    imagen.src = `imgs1/ahorcado_${cimagen}.png`;

    if (cimagen < 0) {
        alert("Has perdido");
    }
    if (puntos >= victoria) {
        alert("Has ganado!");
    }
});


let base=0;
let altura=0;
let perimetro=0;
let area=0;

// Pedimos los datos utilizando parseInt para transformarlos a numeros y realizaar mejor los calculos
base=parseInt(prompt("Introduce la base"));
altura=parseInt(prompt("Introuce la altura"));

// Procesamos los datos
perimetro=2*base+2*altura;
area=base*altura;

// Mostramos los resultados en pantalla
document.write(`El area del circulo es: ${area} <br> El perimetro es: ${perimetro}`);
// Creamos las variable para acceder facilmente a los parrafos HTML
let parrafo1=document.querySelector("#parrafo1"); 
let parrafo2=document.querySelector("#parrafo2");

// Creamos las variables y les asignamos el texto
let mensaje1=("Que facil es usar 'comillas simples'");
let mensaje2=('y "comillas dobles"');

console.log("Hola mundo!");

// Agregamos los textos a los parrafos vacios el HTML
parrafo1.innerHTML=("Esta pagina contiene el primer script"); 
parrafo2.innerHTML=("Soy el primer script");

// Utilizamos las variables dentro de la alerta, el \n es un salto de linea
alert(`Hola mundo!\n ${mensaje1} ${mensaje2}`);
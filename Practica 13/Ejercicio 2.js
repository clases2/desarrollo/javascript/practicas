function mover(hacia) {
    let imagen = document.querySelector('#l1');
    let posicion = window.getComputedStyle(imagen).getPropertyValue('top');
    if (hacia == 'arriba') {
        imagen.style.top = `${parseInt(posicion) - 10}px`;
    } else {
        imagen.style.top = `${parseInt(posicion) + 10}px`;
    }
}
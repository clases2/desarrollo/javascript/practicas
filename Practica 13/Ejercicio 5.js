document.addEventListener("mousemove", function mover(evento) {
    let imagen = document.querySelector('#layer1');
    let alto = window.getComputedStyle(imagen).getPropertyValue('height');
    let ancho = window.getComputedStyle(imagen).getPropertyValue('width');

    imagen.style.top = `${evento.clientY - parseInt(alto) / 2}px`;
    imagen.style.left = `${evento.clientX - parseInt(ancho) / 2}px`;
}); 
document.querySelector('#layer2').addEventListener("mousemove", function mover(evento) {
    let imagen = document.querySelector('#layer1');
    let cuadro = document.querySelector('#layer2');
    let horizontal1 = window.getComputedStyle(cuadro).getPropertyValue('left');
    let vertical1 = window.getComputedStyle(cuadro).getPropertyValue('top');

    imagen.style.top = `${parseInt(evento.clientY) - parseInt(vertical1)}px`;
    imagen.style.left = `${parseInt(evento.clientX) - parseInt(horizontal1)}px`;
});
let boton = document.querySelector('#layer2');

boton.addEventListener("mousedown", function mover(evento) {
    let imagen = document.querySelector('#layer1');
    let posicion = window.getComputedStyle(imagen).getPropertyValue('left');
    if (evento.button == '0') {
        imagen.style.left = `${parseInt(posicion) + 10}px`;
    } else if (evento.button == '2') {
        imagen.style.left = `${parseInt(posicion) - 10}px`;
    }
});
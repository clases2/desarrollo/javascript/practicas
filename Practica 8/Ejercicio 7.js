let contador=0;
let sumador=0;
let dato=0;

dato=+prompt("Introduce un numero para calcular la media");

while(dato!=0){
    contador++;
    sumador=sumador+dato;
    dato=+prompt("Introduce un numero para calcular la media o escribe cero para terminar el proceso");
}

if (contador==0) {
    document.write("La media es cero");
} else {
    document.write(`La media es ${sumador/contador}`);
}
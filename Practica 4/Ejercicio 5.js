// Al crear la variable y escribir dos corchetes se crea un array
let num=[];

// A un array se le puede asignar mas de un numero, el primer numero del array es el 0
num[0]=parseInt(prompt("Introduce el primer numero"));
num[1]=parseInt(prompt("Introduce el segundo numero"));
num[2]=parseInt(prompt("Introduce el tercer numero"));

console.log(`Primer numero: ${num[0]}`);
console.log(`Segundo numero: ${num[1]}`);
console.log(`Tercer numero: ${num[2]}`);

document.write(`Primer numero: ${num[0]}<br> Segundo numero: ${num[1]}<br> tercer numero: ${num[2]}`);
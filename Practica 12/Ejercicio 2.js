function mostrar() {
    let numeros = [];
    for (let cont = 0; cont < 10; cont++) {
        //El comando Math.floor redondea el numero hacia abajo, el Math.random devuelve un numero aleatorio del 0 al 1 sin incluir el 1.
        //Al multiplicar el numero aleatorio por 10 se genera un numero no del 0 al 1 sino del 0 al 10 sin incluir el 10, al sumarle 1 se genera un numero del 1 al 10.
        numeros[cont] = Math.floor((Math.random() * 10) + 1); 
    }

    for (let cont = 0; cont < numeros.length; cont++) {
        document.write(`${numeros[cont]}<br>`);
    }

}

mostrar();
let boton = document.querySelectorAll('button');

for (const botones of boton) {
    botones.addEventListener("click", function (e) {
        click = e.target.innerHTML;
        resultado = document.querySelector('#resultado');
        if (click == 'Sumar') {
            resultado.value = parseInt(document.querySelector('#suma1').value) + parseInt(document.querySelector('#suma2').value);
        } else if (click == 'Restar') {
            resultado.value = parseInt(document.querySelector('#resta1').value) - parseInt(document.querySelector('#resta2').value);
        } else if (click == 'Negar') {
            resultado.value -= parseInt(document.querySelector('#negar').value);
        } else if (click == 'Producto') {
            resultado.value = parseInt(document.querySelector('#producto1').value) * parseInt(document.querySelector('#producto2').value);
        } else if (click == 'Cociente') {
            resultado.value = parseInt(document.querySelector('#cociente1').value) / parseInt(document.querySelector('#cociente2').value);
        } else if (click == 'Resto') {
            resultado.value = parseInt(document.querySelector('#resto1').value) % parseInt(document.querySelector('#resto2').value);
        }
    });
}
document.querySelector('button').addEventListener("click", function (e) {
    numeros = document.querySelectorAll('input');
    resultado = document.querySelector('#resultado');

    multiplicador = parseInt(numeros[0].value);
    minimo = parseInt(numeros[1].value);
    maximo = parseInt(numeros[2].value);

    resultado.innerHTML = '';
    for (let cont = minimo; cont <= maximo; cont++) {
        resultado.innerHTML += `${multiplicador}x${cont}=${multiplicador * cont}<br>`
    }
});

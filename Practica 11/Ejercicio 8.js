let numero = +prompt("Escribe un numero");
let multiplicacion = [];

function tabla(valor) {
    let resultado = [];
    for (let cont = 0; cont <= 10; cont++) {
        resultado[cont] = (`${valor}x${cont}=${valor * cont}<br>`);
    }
    return resultado;
}


function mostrar(valor) {
    for (let cont = 0; cont < valor.length; cont++) {
        document.write(`${valor[cont]}<br>`);
    }
}

multiplicacion = tabla(numero);

mostrar(multiplicacion);